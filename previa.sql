-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: iec
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'1_20160810203728.jpg','Banner exemplo','Lorem ipsum dolor sit amet','#','2016-08-10 20:37:29','2016-08-10 20:37:48'),(2,2,'2_20160810203804.jpg','Lorem Ipsum','Lorem ipsum dolor sit amet','#','2016-08-10 20:38:04','2016-08-10 20:38:04');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','+55 (11) 4317-3101, +55 (11) 4317-3111','<p>Rua Cristiano Angeli, 1238 - Bairro Assun&ccedil;&atilde;o</p>\r\n\r\n<p>09812-600 - S&atilde;o Bernardo do Campo / SP - Brasil</p>\r\n','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.761517065459!2d-46.57749904991609!3d-23.720208984528643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce415fb63d5d53%3A0xe7d22f7b096e478!2sR.+Cristiano+Angeli%2C+1238+-+Assun%C3%A7%C3%A3o%2C+S%C3%A3o+Bernardo+do+Campo+-+SP%2C+09812-600!5e0!3m2!1spt-BR!2sbr!4v1470861697595\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,'2016-08-10 20:35:28');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escopo_de_negocios`
--

DROP TABLE IF EXISTS `escopo_de_negocios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escopo_de_negocios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escopo_de_negocios`
--

LOCK TABLES `escopo_de_negocios` WRITE;
/*!40000 ALTER TABLE `escopo_de_negocios` DISABLE KEYS */;
INSERT INTO `escopo_de_negocios` VALUES (1,1,'PLANEJAMENTO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed massa vitae leo aliquam accumsan. Nam a elit nec lacus egestas elementum bibendum in augue.\r\n','2016-08-10 20:33:10','2016-08-10 20:33:10'),(2,2,'CRIAÇÃO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed massa vitae leo aliquam accumsan. Nam a elit nec lacus egestas elementum bibendum in augue.\r\n','2016-08-10 20:33:19','2016-08-10 20:33:19'),(3,3,'INSTALAÇÃO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed massa vitae leo aliquam accumsan. Nam a elit nec lacus egestas elementum bibendum in augue.\r\n','2016-08-10 20:33:25','2016-08-10 20:33:25');
/*!40000 ALTER TABLE `escopo_de_negocios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_categoria_foreign` (`categoria`),
  CONSTRAINT `home_categoria_foreign` FOREIGN KEY (`categoria`) REFERENCES `produtos_categorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` VALUES (1,1,1,'img-institucioal_20160810204124.png','2016-08-10 20:41:25','2016-08-10 20:41:25'),(2,2,2,'img-institucioal_20160810204130.png','2016-08-10 20:41:30','2016-08-10 20:41:30'),(3,3,3,'img-institucioal_20160810204139.png','2016-08-10 20:41:39','2016-08-10 20:41:39'),(4,4,4,'img-institucioal_20160810204143.png','2016-08-10 20:41:43','2016-08-10 20:41:43');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institucional`
--

DROP TABLE IF EXISTS `institucional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_institucional` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_localizacao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_localizacao_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_localizacao_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institucional`
--

LOCK TABLES `institucional` WRITE;
/*!40000 ALTER TABLE `institucional` DISABLE KEYS */;
INSERT INTO `institucional` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed massa vitae leo aliquam accumsan. Nam a elit nec lacus egestas elementum bibendum in augue. Phasellus ac augue lacinia, convallis dui sit amet, sagittis augue. Nulla vitae diam suscipit, facilisis enim non, malesuada ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis velit venenatis metus tempor cursus nec nec lorem. Quisque volutpat gravida sem, quis suscipit dui accumsan ac. Donec at mi sed tortor malesuada mollis. Vestibulum semper lorem nec felis suscipit iaculis. Pellentesque facilisis vestibulum metus, nec varius tellus euismod cursus. Quisque pellentesque, ex sit amet bibendum pharetra, massa nibh eleifend magna, eget bibendum urna justo et erat. Sed eget ultrices justo. Integer tincidunt malesuada dapibus.</p>\r\n\r\n<p>Sed eleifend velit tellus, quis sodales orci dignissim pulvinar. Morbi lorem ex, egestas eu erat quis, ullamcorper commodo nunc. Cras porttitor, purus nec efficitur posuere, purus lacus sagittis dui, eget varius magna diam ac nisi. Vestibulum auctor tincidunt dolor, ac euismod nisl ornare sed. Phasellus tincidunt sollicitudin massa vel blandit. Pellentesque tincidunt finibus ullamcorper. Morbi tincidunt et sem at iaculis. Vivamus mauris quam, consectetur nec eleifend id, aliquet id est</p>\r\n','img-institucioal_20160810203201.png','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed massa vitae leo aliquam accumsan. Nam a elit nec lacus egestas elementum bibendum in augue. Phasellus ac augue lacinia, convallis dui sit amet, sagittis augue.</p>\r\n','img-localizacao1_20160810203201.png','img-localizacao2_20160810203201.png',NULL,'2016-08-10 20:32:30');
/*!40000 ALTER TABLE `institucional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_08_05_161209_create_banners_table',1),('2016_08_05_161648_create_institucional_table',1),('2016_08_05_162115_create_escopo_de_negocios_table',1),('2016_08_05_162334_create_obras_executadas_table',1),('2016_08_05_210648_create_produtos_categorias_table',1),('2016_08_08_214338_create_home_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obras_executadas`
--

DROP TABLE IF EXISTS `obras_executadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obras_executadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obras_executadas`
--

LOCK TABLES `obras_executadas` WRITE;
/*!40000 ALTER TABLE `obras_executadas` DISABLE KEYS */;
INSERT INTO `obras_executadas` VALUES (1,0,'img-institucioal_20160810203402.png','Exemplo','2016-08-10 20:34:02','2016-08-10 20:34:02');
/*!40000 ALTER TABLE `obras_executadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_categorias`
--

DROP TABLE IF EXISTS `produtos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_categorias`
--

LOCK TABLES `produtos_categorias` WRITE;
/*!40000 ALTER TABLE `produtos_categorias` DISABLE KEYS */;
INSERT INTO `produtos_categorias` VALUES (1,1,'Equipamentos de pintura','2016-08-10 20:38:34','2016-08-10 20:38:34'),(2,2,'Equipamentos para fluidos de alta viscosidade','2016-08-10 20:38:55','2016-08-10 20:38:55'),(3,3,'Sistemas de controle de fluxo','2016-08-10 20:39:04','2016-08-10 20:39:04'),(4,4,'Sistemas de controle de temperatura','2016-08-10 20:39:15','2016-08-10 20:39:15');
/*!40000 ALTER TABLE `produtos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_itens`
--

DROP TABLE IF EXISTS `produtos_itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_itens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produto_subcategoria_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produtos_itens_produto_subcategoria_id_foreign` (`produto_subcategoria_id`),
  CONSTRAINT `produtos_itens_produto_subcategoria_id_foreign` FOREIGN KEY (`produto_subcategoria_id`) REFERENCES `produtos_subcategorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_itens`
--

LOCK TABLES `produtos_itens` WRITE;
/*!40000 ALTER TABLE `produtos_itens` DISABLE KEYS */;
INSERT INTO `produtos_itens` VALUES (1,1,0,'Exemplo','1_20160810204002.jpg','2016-08-10 20:40:03','2016-08-10 20:40:03'),(2,2,0,'Exemplo','2_20160810204015.jpg','2016-08-10 20:40:16','2016-08-10 20:40:16'),(3,3,0,'Exemplo','1_20160810204056.jpg','2016-08-10 20:40:57','2016-08-10 20:40:57');
/*!40000 ALTER TABLE `produtos_itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_subcategorias`
--

DROP TABLE IF EXISTS `produtos_subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_subcategorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produto_categoria_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produtos_subcategorias_produto_categoria_id_foreign` (`produto_categoria_id`),
  CONSTRAINT `produtos_subcategorias_produto_categoria_id_foreign` FOREIGN KEY (`produto_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_subcategorias`
--

LOCK TABLES `produtos_subcategorias` WRITE;
/*!40000 ALTER TABLE `produtos_subcategorias` DISABLE KEYS */;
INSERT INTO `produtos_subcategorias` VALUES (1,1,1,'Bombas airless','2016-08-10 20:39:47','2016-08-10 20:39:47'),(2,1,2,'Pistolas','2016-08-10 20:39:53','2016-08-10 20:39:53'),(3,2,0,'Bombas de alta viscosidade','2016-08-10 20:40:47','2016-08-10 20:40:47');
/*!40000 ALTER TABLE `produtos_subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$OuVsn5QPqAlCTYpI3x7EV.qVPY56ANsxARb99FVBIYzAm.PXdKiLm',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-10 20:49:45
