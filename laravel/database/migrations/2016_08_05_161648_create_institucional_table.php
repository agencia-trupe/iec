<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_institucional');
            $table->string('imagem_institucional');
            $table->text('texto_localizacao');
            $table->string('imagem_localizacao_1');
            $table->string('imagem_localizacao_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
