<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscopoDeNegociosTable extends Migration
{
    public function up()
    {
        Schema::create('escopo_de_negocios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('escopo_de_negocios');
    }
}
