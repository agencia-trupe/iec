<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('categoria')->unsigned();
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('categoria')->references('id')->on('produtos_categorias')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
