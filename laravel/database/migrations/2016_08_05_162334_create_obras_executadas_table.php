<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObrasExecutadasTable extends Migration
{
    public function up()
    {
        Schema::create('obras_executadas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('legenda');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('obras_executadas');
    }
}
