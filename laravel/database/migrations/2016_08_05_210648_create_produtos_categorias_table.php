<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosCategoriasTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->timestamps();
        });

        Schema::create('produtos_subcategorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_categoria_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->timestamps();
            $table->foreign('produto_categoria_id')->references('id')->on('produtos_categorias')->onDelete('cascade');
        });

        Schema::create('produtos_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_subcategoria_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_subcategoria_id')->references('id')->on('produtos_subcategorias')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('produtos_itens');
        Schema::drop('produtos_subcategorias');
        Schema::drop('produtos_categorias');
    }
}
