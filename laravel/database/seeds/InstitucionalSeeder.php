<?php

use Illuminate\Database\Seeder;

class InstitucionalSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'texto_institucional' => '',
            'imagem_institucional' => '',
            'texto_localizacao' => '',
            'imagem_localizacao_1' => '',
            'imagem_localizacao_2' => '',
        ]);
    }
}
