@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Escopo de Negócios /</small> Editar Escopo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.escopo-de-negocios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.escopo-de-negocios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
