@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Escopo de Negócios /</small> Adicionar Escopo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.escopo-de-negocios.store', 'files' => true]) !!}

        @include('painel.escopo-de-negocios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
