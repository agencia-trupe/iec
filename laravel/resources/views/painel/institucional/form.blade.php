@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_institucional', 'Texto Institucional') !!}
    {!! Form::textarea('texto_institucional', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_institucional', 'Imagem Institucional') !!}
    <img src="{{ url('assets/img/institucional/'.$registro->imagem_institucional) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_institucional', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_localizacao', 'Texto Localização') !!}
    {!! Form::textarea('texto_localizacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_localizacao_1', 'Imagem Localização 1') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_localizacao_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_localizacao_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_localizacao_2', 'Imagem Localização 2') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_localizacao_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_localizacao_2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
