<ul class="nav navbar-nav">
	<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.home.index') }}">Home</a>
	</li>
	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    <li @if(str_is('painel.institucional*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.institucional.index') }}">Institucional</a>
    </li>
    <li @if(str_is('painel.escopo-de-negocios*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.escopo-de-negocios.index') }}">Escopo de Negócios</a>
    </li>
    <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li @if(str_is('painel.obras-executadas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.obras-executadas.index') }}">Obras Executadas</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
