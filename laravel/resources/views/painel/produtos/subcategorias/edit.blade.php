@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $categoria->titulo }} /</small> Editar Subcategoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.subcategorias.update', $categoria->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.subcategorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
