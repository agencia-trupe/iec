@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $categoria->titulo }} /</small> Adicionar Subcategoria</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.subcategorias.store', $categoria->id], 'files' => true]) !!}

        @include('painel.produtos.subcategorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
