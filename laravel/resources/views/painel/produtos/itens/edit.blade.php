@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $categoria->titulo }} / {{ $subcategoria->titulo }} /</small> Editar Item</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.subcategorias.itens.update', $categoria->id, $subcategoria->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.itens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
