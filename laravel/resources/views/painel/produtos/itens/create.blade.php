@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $categoria->titulo }} / {{ $subcategoria->titulo }} /</small> Adicionar Item</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.subcategorias.itens.store', $categoria->id, $subcategoria->id], 'files' => true]) !!}

        @include('painel.produtos.itens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
