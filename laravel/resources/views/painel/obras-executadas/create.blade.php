@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Obras Executadas /</small> Adicionar Obra</h2>
    </legend>

    {!! Form::open(['route' => 'painel.obras-executadas.store', 'files' => true]) !!}

        @include('painel.obras-executadas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
