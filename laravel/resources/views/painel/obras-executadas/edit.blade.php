@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Obras Executadas /</small> Editar Obra</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.obras-executadas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.obras-executadas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
