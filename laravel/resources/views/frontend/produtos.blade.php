@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <h1>PRODUTOS E SOLUÇÕES</h1>
            <div class="breadcrumb"><span>PRODUTOS E SOLUÇÕES</span></div>

            @foreach($categorias as $categoria)
            <div class="produtos-categoria @if($selecao == $categoria->id) aberto @endif">
                <a href="#" class="titulo">{{ $categoria->titulo }}</a>
                @foreach($categoria->subcategorias as $subcategoria)
                <div class="produtos-subcategoria">
                    <h3 class="sub-titulo">{{ $subcategoria->titulo }}</h3>
                    <div class="produtos-itens">
                        @foreach($subcategoria->itens as $item)
                        <a href="{{ asset('assets/img/produtos/ampliado/'.$item->imagem) }}" class="produto fancybox">
                            <img src="{{ asset('assets/img/produtos/'.$item->imagem) }}" alt="">
                            <span>{{ $item->titulo }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>

@endsection
