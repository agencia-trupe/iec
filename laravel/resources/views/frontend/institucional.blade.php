@extends('frontend.common.template')

@section('content')

    <div class="main institucional">
        <div class="center">
            <h1>IEC DO BRASIL LTDA.</h1>
            <div class="breadcrumb"><span>INSTITUCIONAL</span></div>
            <div class="conteudo-institucional">
                <div class="texto">
                    {!! $institucional->texto_institucional !!}
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/institucional/'.$institucional->imagem_institucional) }}" alt="">
                </div>
            </div>

            <div class="breadcrumb"><span>LOCALIZAÇÃO</span></div>
            <div class="conteudo-localizacao">
                <div class="texto">
                    {!! $institucional->texto_localizacao !!}
                </div>
                <div class="imagens">
                    <img src="{{ asset('assets/img/institucional/'.$institucional->imagem_localizacao_1) }}" alt="">
                    <img src="{{ asset('assets/img/institucional/'.$institucional->imagem_localizacao_2) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
