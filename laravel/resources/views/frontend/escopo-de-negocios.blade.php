@extends('frontend.common.template')

@section('content')

    <div class="main escopo-de-negocios">
        <div class="center">
            <h1>ESCOPO DE NEGÓCIOS</h1>
            <div class="breadcrumb"><span>ESCOPO DE NEGÓCIOS</span></div>

            <div class="escopos">
                @foreach($escopos as $escopo)
                <div>
                    <h2>{{ $escopo->titulo }}</h2>
                    <p>{{ $escopo->texto }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
