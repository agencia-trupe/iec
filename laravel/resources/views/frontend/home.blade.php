@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="center">
                    <div class="banner-texto">
                        <h2>{{ $banner->titulo }}</h2>
                        <p>{{ $banner->subtitulo }}</p>
                    </div>
                </div>
            </a>
            @endforeach

            <div id="banners-pager">
                @foreach($banners as $banner)
                <a href="#">{{ $banner->titulo }}</a>
                @endforeach
            </div>
        </div>

        <div class="center">
            <p class="frase">Enxergar, atender e aprimorar as necessidades na área industrial</p>
            <div class="chamadas">
                @foreach($chamadas as $chamada)
                <a href="{{ route('produtos', ['cat' => $chamada->cat->id]) }}">
                    <span>{{ $chamada->cat->titulo }}</span>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/home/'.$chamada->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
