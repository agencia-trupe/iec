@extends('frontend.common.template')

@section('content')

    <div class="main obras-executadas">
        <div class="center">
            <h1>OBRAS EXECUTADAS</h1>
            <div class="breadcrumb"><span>OBRAS EXECUTADAS</span></div>

            <div class="thumbs">
                @foreach($obras as $obra)
                <a href="{{ asset('assets/img/obras-executadas/ampliado/'.$obra->imagem) }}" class="fancybox">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/obras-executadas/'.$obra->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </div>
                    <span>{{ $obra->legenda }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
