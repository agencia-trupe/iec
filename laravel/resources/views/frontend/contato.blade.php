@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h1>CONTATO</h1>
            <div class="breadcrumb"><span>CONTATO</span></div>

            <div class="informacoes">
                <p class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <span>{{ $telefone }}</span>
                    @endforeach
                </p>
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>

            <form action="" id="form-contato" method="POST">
                <div id="form-contato-response"></div>
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
