<a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>
    HOME
</a>
<a href="{{ route('institucional') }}" @if(Route::currentRouteName() === 'institucional') class="active" @endif>
    INSTITUCIONAL
</a>
<a href="{{ route('escopo-de-negocios') }}" @if(Route::currentRouteName() === 'escopo-de-negocios') class="active" @endif">
    ESCOPO DE NEGÓCIOS
</a>
<a href="{{ route('produtos') }}" @if(Route::currentRouteName() === 'produtos') class="active" @endif>
    PRODUTOS E SOLUÇÕES
</a>
<a href="{{ route('obras-executadas') }}" @if(Route::currentRouteName() === 'obras-executadas') class="active" @endif>
    OBRAS EXECUTADAS
</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif">
    CONTATO
</a>
