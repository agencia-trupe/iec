    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}">&raquo; HOME</a>
                <a href="{{ route('institucional') }}">&raquo; INSTITUCIONAL</a>
                <a href="{{ route('escopo-de-negocios') }}">&raquo; ESCOPO DE NEGÓCIOS</a>
                <a href="{{ route('obras-executadas') }}">&raquo; OBRAS EXECUTADAS</a>
            </div>

            <div class="col">
                <a href="{{ route('produtos') }}">&raquo; PRODUTOS E SOLUÇÕES</a>
                @foreach($categorias as $categoria)
                <a href="{{ route('produtos', ['cat' => $categoria->id]) }}" class="categoria">&middot; {{ $categoria->titulo }}</a>
                @endforeach
            </div>

            <div class="col">
                <a href="{{ route('contato') }}">&raquo; CONTATO</a>
                <p class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <span>{{ $telefone }}</span>
                    @endforeach
                </p>
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
