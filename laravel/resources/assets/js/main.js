(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        $('.banners').cycle({
            slides: '>.slide',
            pager: '#banners-pager',
            pagerTemplate: ''
        });
    };

    App.thumbFancybox = function() {
        $('.fancybox').fancybox({
            padding: 0,
        });
    };

    App.produtos = function() {
        $('.produtos-categoria > .titulo').click(function(event) {
            event.preventDefault();
            $(this).parent().toggleClass('aberto');

            if ($(this).parent().hasClass('aberto')) {
                $('html, body').animate({
                    scrollTop: $(this).parent().offset().top
                }, 500);
            }
        });

        $('.sub-titulo').click(function(event) {
            event.preventDefault();

            if ($(this).parent().parent().hasClass('aberto')) return;
            $(this).parent().parent().toggleClass('aberto');

            if ($(this).parent().parent().hasClass('aberto')) {
                $('html, body').animate({
                    scrollTop: $(this).offset().top
                }, 500);
            }
        });

        var aberto = $('.produtos-categoria.aberto');
        if (aberto.length == 1) {
            $('html, body').animate({
                scrollTop: aberto.offset().top
            }, 500);
        }
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.thumbFancybox();
        this.produtos();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
