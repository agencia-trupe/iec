<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('institucional', 'InstitucionalController@index')->name('institucional');
    Route::get('escopo-de-negocios', 'EscopoDeNegociosController@index')->name('escopo-de-negocios');
    Route::get('produtos-e-solucoes', 'ProdutosController@index')->name('produtos');
    Route::get('obras-executadas', 'ObrasExecutadasController@index')->name('obras-executadas');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@envio')->name('contato.envio');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('home', 'HomeController');
        Route::resource('produtos', 'ProdutosCategoriasController');
        Route::resource('produtos.subcategorias', 'ProdutosSubcategoriasController');
		Route::resource('produtos.subcategorias.itens', 'ProdutosItensController');
		Route::resource('obras-executadas', 'ObrasExecutadasController');
		Route::resource('escopo-de-negocios', 'EscopoDeNegociosController');
		Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
