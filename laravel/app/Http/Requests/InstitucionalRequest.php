<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_institucional' => 'required',
            'imagem_institucional' => 'image',
            'texto_localizacao' => 'required',
            'imagem_localizacao_1' => 'image',
            'imagem_localizacao_2' => 'image',
        ];
    }
}
