<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosSubcategoriasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
