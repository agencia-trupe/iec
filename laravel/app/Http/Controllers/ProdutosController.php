<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ProdutoCategoria;

class ProdutosController extends Controller
{
    public function index(Request $request)
    {
        $categorias = ProdutoCategoria::ordenados()->get();
        $selecao    = $request->has('cat') ? (int)$request->get('cat') : 0;

        return view('frontend.produtos', compact('categorias', 'selecao'));
    }
}
