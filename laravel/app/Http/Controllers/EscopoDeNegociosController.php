<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Escopo;

class EscopoDeNegociosController extends Controller
{
    public function index()
    {
        $escopos = Escopo::ordenados()->get();

        return view('frontend.escopo-de-negocios', compact('escopos'));
    }
}
