<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HomeRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamada;
use App\Models\ProdutoCategoria;

class HomeController extends Controller
{
    public function index()
    {
        $registros = Chamada::ordenados()->get();

        return view('painel.home.index', compact('registros'));
    }

    public function create()
    {
        $categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.home.create', compact('categorias'));
    }

    public function store(HomeRequest $request)
    {
        try {

            if (Chamada::count() >= 4) {
                throw new \Exception('Limite de registros excedido', 1);
            }

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Chamada::upload_imagem();

            Chamada::create($input);
            return redirect()->route('painel.home.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Chamada $registro)
    {
        $categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.home.edit', compact('registro', 'categorias'));
    }

    public function update(HomeRequest $request, Chamada $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Chamada::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Chamada $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.home.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
