<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;

class ProdutosCategoriasController extends Controller
{
    public function index()
    {
        $registros = ProdutoCategoria::ordenados()->get();

        return view('painel.produtos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos.create');
    }

    public function store(ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->all();


            ProdutoCategoria::create($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoCategoria $registro)
    {
        return view('painel.produtos.edit', compact('registro'));
    }

    public function update(ProdutosCategoriasRequest $request, ProdutoCategoria $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoCategoria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
