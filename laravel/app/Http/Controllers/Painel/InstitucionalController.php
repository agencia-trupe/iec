<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InstitucionalRequest;
use App\Http\Controllers\Controller;

use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_institucional'])) $input['imagem_institucional'] = Institucional::upload_imagem_institucional();
            if (isset($input['imagem_localizacao_1'])) $input['imagem_localizacao_1'] = Institucional::upload_imagem_localizacao_1();
            if (isset($input['imagem_localizacao_2'])) $input['imagem_localizacao_2'] = Institucional::upload_imagem_localizacao_2();

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
