<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosItensRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\ProdutoSubcategoria;
use App\Models\ProdutoItem;

class ProdutosItensController extends Controller
{
    public function index(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria)
    {
        $registros = $subcategoria->itens()->get();

        return view('painel.produtos.itens.index', compact('registros', 'categoria', 'subcategoria'));
    }

    public function create(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria)
    {
        return view('painel.produtos.itens.create', compact('categoria', 'subcategoria'));
    }

    public function store(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria, ProdutosItensRequest $request)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = ProdutoItem::upload_imagem();

            $subcategoria->itens()->create($input);

            return redirect()->route('painel.produtos.subcategorias.itens.index', [$categoria->id, $subcategoria->id])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria, ProdutoItem $registro)
    {
        return view('painel.produtos.itens.edit', compact('registro', 'categoria', 'subcategoria'));
    }

    public function update(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria, ProdutosItensRequest $request, ProdutoItem $registro)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = ProdutoItem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.subcategorias.itens.index', [$categoria->id, $subcategoria->id])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoCategoria $categoria, ProdutoSubcategoria $subcategoria, ProdutoItem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.produtos.subcategorias.itens.index', [$categoria->id, $subcategoria->id])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
