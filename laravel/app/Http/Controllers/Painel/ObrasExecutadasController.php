<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ObrasExecutadasRequest;
use App\Http\Controllers\Controller;

use App\Models\Obra;

class ObrasExecutadasController extends Controller
{
    public function index()
    {
        $registros = Obra::ordenados()->get();

        return view('painel.obras-executadas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.obras-executadas.create');
    }

    public function store(ObrasExecutadasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Obra::upload_imagem();

            Obra::create($input);
            return redirect()->route('painel.obras-executadas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Obra $registro)
    {
        return view('painel.obras-executadas.edit', compact('registro'));
    }

    public function update(ObrasExecutadasRequest $request, Obra $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Obra::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.obras-executadas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Obra $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.obras-executadas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
