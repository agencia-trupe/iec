<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EscopoDeNegociosRequest;
use App\Http\Controllers\Controller;

use App\Models\Escopo;

class EscopoDeNegociosController extends Controller
{
    public function index()
    {
        $registros = Escopo::ordenados()->get();

        return view('painel.escopo-de-negocios.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.escopo-de-negocios.create');
    }

    public function store(EscopoDeNegociosRequest $request)
    {
        try {

            $input = $request->all();


            Escopo::create($input);
            return redirect()->route('painel.escopo-de-negocios.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Escopo $registro)
    {
        return view('painel.escopo-de-negocios.edit', compact('registro'));
    }

    public function update(EscopoDeNegociosRequest $request, Escopo $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.escopo-de-negocios.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Escopo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.escopo-de-negocios.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
