<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosSubcategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\ProdutoSubcategoria;

class ProdutosSubcategoriasController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        $registros = $categoria->subcategorias()->get();

        return view('painel.produtos.subcategorias.index', compact('registros', 'categoria'));
    }

    public function create(ProdutoCategoria $categoria)
    {
        return view('painel.produtos.subcategorias.create', compact('categoria'));
    }

    public function store(ProdutoCategoria $categoria, ProdutosSubcategoriasRequest $request)
    {
        try {

            $input = $request->all();
            $categoria->subcategorias()->create($input);

            return redirect()->route('painel.produtos.subcategorias.index', $categoria->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoCategoria $categoria, ProdutoSubcategoria $registro)
    {
        return view('painel.produtos.subcategorias.edit', compact('registro', 'categoria'));
    }

    public function update(ProdutoCategoria $categoria, ProdutosSubcategoriasRequest $request, ProdutoSubcategoria $registro)
    {
        try {

            $input = $request->all();
            $registro->update($input);

            return redirect()->route('painel.produtos.subcategorias.index', $categoria->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoCategoria $categoria, ProdutoSubcategoria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.produtos.subcategorias.index', $categoria->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
