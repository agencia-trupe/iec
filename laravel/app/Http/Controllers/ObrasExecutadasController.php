<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Obra;

class ObrasExecutadasController extends Controller
{
    public function index()
    {
        $obras = Obra::ordenados()->get();

        return view('frontend.obras-executadas', compact('obras'));
    }
}
