<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoSubcategoria extends Model
{
    protected $table = 'produtos_subcategorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produto_categoria_id');
    }

    public function itens()
    {
        return $this->hasMany('App\Models\ProdutoItem', 'produto_subcategoria_id')->ordenados();
    }
}
