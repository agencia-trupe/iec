<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_imagem_institucional()
    {
        return CropImage::make('imagem_institucional', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_localizacao_1()
    {
        return CropImage::make('imagem_localizacao_1', [
            'width'  => 460,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_localizacao_2()
    {
        return CropImage::make('imagem_localizacao_2', [
            'width'  => 460,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

}
