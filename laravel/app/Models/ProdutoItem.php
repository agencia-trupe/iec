<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoItem extends Model
{
    protected $table = 'produtos_itens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function subcategoria()
    {
        return $this->belongsTo('App\Models\ProdutoSubcategoria', 'produto_subcategoria_id');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 180,
                'height' => 200,
                'bg'     => '#fff',
                'path'   => 'assets/img/produtos/'
            ],
            [
                'width'  => 980,
                'height' => null,
                'upsize' => true,
                'path'   => 'assets/img/produtos/ampliado/'
            ]
        ]);
    }
}
