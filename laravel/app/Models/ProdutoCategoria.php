<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoCategoria extends Model
{
    protected $table = 'produtos_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function subcategorias()
    {
        return $this->hasMany('App\Models\ProdutoSubcategoria', 'produto_categoria_id')->ordenados();
    }
}
