<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Obra extends Model
{
    protected $table = 'obras_executadas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 360,
                'height' => 220,
                'path'   => 'assets/img/obras-executadas/'
            ],
            [
                'width'  => 980,
                'height' => null,
                'upsize' => true,
                'path'   => 'assets/img/obras-executadas/ampliado/'
            ]
        ]);
    }

}
